Song: Never Forgotten Heroes
Artist: Rhapsody (www.mightyrhapsody.com)
Album: Symphony Of Enchanted Lands II -The Dark Secret-
BPM:180
Steps: Skorpion9x
Graphics: Skorpion9x - image used is a wallpaper downloaded from Rhapsody's official site. It was provided to them from Chava (http://www.rhapsody.es.vg on the image, but as of this writing, I get a 404 when I try to access the site). Aside from adding text and resizing, the image is otherwise unaltered.
MP3: Ripped and shortened by Skorpion9x - I cut a minute or so off of the beginning, which included some choir singers and a slow 30 second piano part. I figured a lot or people would complain that it was boring and should have been cut off, so I did. 

Here's another awesome tune from the mighty Rhapsody's latest album! This shortened mp3 clocks in at just under four and a half minutes. With a max combo of 2210, this sim isn't as streamy as Emerald Sword. The only really long streams are during the choruses (three total). There is also an awesome keyboard solo! Steps go mostly to the drums (of course :)), as well as some other melodies and guitars. Keyboard only.

-Skorpion9x

Join me for some SMOnline (Name: Skor) :D
Forums: Skorpion9x (Skor on SMOnline)