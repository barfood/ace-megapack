Title:     CLASSICAL INSANITY
Artist:    DM Ashura
Steps:     DR
BG/Banner: DR
BPM:       175

A remix of nine classical songs done by the masterful DM Ashura (you know, the
guy who made neoMAX?). Anyways, the songs are, in order of appearance...

Revolutionary Etude by Fr�d�ric F. Chopin
Moonlight Sonata, Mvt. 3 by Ludwig van Beethoven
Turkish March by Wolfgang A. Mozart
Hungarian Dance No. 5 by Johannes Brahms
Csikos Post by Hermann Necke
Summer, Mvt. 3 from The Four Seasons by Antonio Vivaldi
Sonata Pathetique, Mvt. 3 (Rondo) by Ludwig van Beethoven
Winter, Mvt. 1 from The Four Seasons by Antonio Vivaldi
Flight of the Bumblebee by Nikolai Rimsky-Korsakov

Which in turn may all be alternately known as Kakumei (DDR), M (?), Turkey
March (Pump) and also part of Classic Party 2 (GF/DM), Brahms (O2Jam), Csikos
Post (Pump), [unknown?], Beethoven Virus (Pump), V (IIDX/DDR), and finally,
part of Classic Party Revival (GF/DM).

Needless to say, given the source material and the arranger, this is definitely
an extremely fun song, with steps that I've attempted to do justice to. Enjoy!




Light/Basic
  3/10

  Very simple, but still very fun. Lots of pressing the same arrow in sequence,
  but a great time could be had with Light on a pad.

Standard/Trick
  7/10

  It isn't a very hard 7. It almost reminds one of sync(EXTREME version), with all
  the crossovers and turns and whatever. Consider it a warmup for Heavy.

Heavy/Maniac
  9/10

  A very difficult 9, but not nearly into 10 territory; the most difficult thing
  you'll encounter is a long run with lots and lots of crossovers; it should
  remind you of the third quarter of Tsugaru Heavy. Plenty of fun jumps too,
  particularly during Csikos Post. Winter has a few 1/16 machineguns, but nothing
  all too tricky.

Oni/SManiac
  12/10!

  KEYBOARD STEPS. Yes, for the first time ever I've made keyboard steps to one
  of my files, despite my iron "pad only" rule. Though, given a bar, they might
  even be possible on foot, but I'll believe it when I see it. Tons and tons of
  1/16s and even a few "handplants" round out the max combo to 1000 even. These
  are done partly as an homage to Eggman, and party because Ashura himself said
  he wanted to submit the song to In The Groove, so these are what I imagine
  ITG's Expert steps would be like (without mines).




dr at bazza dot com

EOF