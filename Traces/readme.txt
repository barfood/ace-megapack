Traces
----------------------------

Author:  TaQ
Album:   Beatmania IIDX 7th Style

DWI Created by: Paige Roozbeh aka. dj REN
E-Mail:         RenX121@hotmail.com
AIM:		x dj ReN x

Single:      Light (4), Standard (7), Heavy (9), Challenge (10)

(shoutout to Cynic for always being there for my DWI needs)

____________________________________________________________________

Light:

My intention in creating these steps were for it to be all in a sort of symmetric pattern and easy to PA as well. These steps actually have the most jumps out of any other sets. A minimal amount of freeze arrows are put in. Overall pretty fun and for people who are new to dwi, it is a great challenge.


Standard:

A bit more difficult than light mode. Features more chains of 1/8th notes and freeze arrows. Off-beats are better introduced to get a better feel to the song and also to add more difficulty. A combination of patters that appear are more extensive. I wanted to get these steps to a NEAR-accurate difficulty as of the arcade would have them.


Heavy:

The combination of everything made use into these steps. Freeze arrows are greatly taken advantage of and a higer level of stream and voltage. A high amount of background beats of the song and much longer chains are are incorporated into the steps. Definitely do'able on a pad, yet very tiring. ( Great way to test your PA skills =D )


Challenge:

OK...this is where i go ALL-OUT IIDX on the song ^^;; ... Almost every beat (if not all) of the song is taken advantage of with massive chains of 1/16ths, freeze arrows, off-beats, and yes, even 1/32nds on the slowdowns, in which i encourage you to play these steps on atleast 3x speed (i personally recommend 4x). It took me a while to figure out the use of 32nds being that it was my first time working with them, but the steps came out excellent. Good Luck!

____________________________________________________________________


Background:

         - Made by Cynic
         - Program used: Adobe Photoshop 7.0


Tools used:

     -Notepad, for writing the steps.
     -YADT (Yet Another DWI Tester)
            Created by: uKER
     -Winamp, used rarely just to listen to upcoming beats in the song.

(yea, i basically just used notepad overall ^^;; yes i keep all the beats in my head and write them down)


About the Song: TaQ - Traces

Definitely, my favorite song by TaQ who is already one of my favorite artists in Bemani. I loved this song too much not to enter it into DDRei's Tournamix 4. It debuted in Beatmania IIDX 7th style with a 7-star rating and is overall enjoyed by many people who have played it. ^__^;;


About dj REN:

        Hi, im dj REN. I am 16 years old and I have been playing DDR for about 1 year and 6 months. I have been playing DWI for a little over a year. I live in Southern California in the San Fernando Valley (part of Los Angeles). My other Bemani interests are Beatmania and Beatmania IIDX. Other games i enjoy are Soul Calibur II, Initial D, and others I cannot think of. I also enjoy scratching and mixing on turntables with various vinyls that i like.



I really enjoyed making this dwi (actually, not really, hehe). I would ultimately appreciate comments and feedback from anyone. Again my contact info:

AIM - x dj REN x
DDRei Username - dj REN
Email - RenX121@hotmail.com