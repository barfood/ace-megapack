This is my second simfile ever. I made it for Arch0wl's Lord of the Fingerdance contest thing, which eventually got called off (;_;). I'm not quite sure why I decided all the sudden to enter a keyboard simfile contest, when I'd never made a keyboard-only file before. I don't have any experiance or skills built up; I barely even play SM. Regardless, I decided to give it my best shot, and I'm pleased with the outcome. I've probably spent well over 10 (maybe a total of 15) hours on this file (as opposed to the 2 or 3 I spent on my first, SA-DA-ME), and I hope the amount of work I've put into it is apparent to people other than myself.

The song is Ray of Illuminati by [esti], from the online game DJ Max. The first DJ Max song I heard, and it's still my favorite. Originally I had toyed with the idea of making Hard Chain Recation from Valkyrie Profile my entry for LotFD, but when I heard Ray of Illuminati I knew it would work much better. Most of the rhythms weren't too tough to figure out, though one section (somewhere around measures 18-25) gave me quite a bit of trouble, but I worked them out in the end. 

The Heavy steps probably aren't perfect; I only threw them together because LotFD required me to. Once it was called off, I stopped working on the Heavy steps. If I was to go back and finish them, I'd probably start over from scratch and do a set that's about 8 feet, or so.

The Challange steps are a 12. Lots of 1/16 notes with some jumps thrown in, but they're not too hard to do with indexes or one handed (I only play 4 fingered, but I was able to AA it both with indexes and one handed without too much trouble, so they're by no means too hard to do).

I got both the MP3 and background from www.m-g-z.com . As of now, the MP3 is STILL missing a second or so from the end (nothing significant, just some fadeout at the end), since downloading anything from MGZ is a pain in the ass. I tried downloading it over 10 times, and I consider myself pretty damn lucky to get as much as I did before they took the MP3 down. If anyone can find me a better (IE: complete) MP3, I'd love you forever.

Big thanks to rob_bot for making the really neat banner for me; I know that if I did it myself I would probably end up submitting something I made in Paint. Thanks a million, rob.

And a big thanks to you, for playing. I'm always open to comments, praise, suggestions, etc. Remember, I'm still pretty new at this, so I can use all the suggestions you can give me. If you want to contact me...

AIM: little heero yuy
MSN: sailordeathscythe@Hotmail.com
email: cstarflare@gmail.com (Don't expect a quick response; I check it maybe once every 2 weeks)
Or you could PM me at DDRFreak.com, Bemanistyle.com, or Arch0wl.com